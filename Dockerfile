# 基础镜像
FROM node:16.14.2

# 设置工作目录
WORKDIR /app

ARG NODE_ENV


#设置镜像源
RUN ls -la && npm install cnpm -g --registry=https://registry.npm.taobao.org

## 安装全局依赖,这个依赖是node-canvas的前置依赖
RUN cnpm install -g node-gyp

# 安装本地依赖
RUN cnpm install --unsafe-perm --canvas_binary_host_mirror="https://registry.npmmirror.com/-/binary/canvas"

# 安装cli-service
RUN cnpm install @vue/cli-service@4.5.13 @vue/cli-plugin-babel -D


# 列出当前目录
RUN ls -la && cat package.json



# 设置环境变量
ENV NODE_ENV=$NODE_ENV VUE_CLI_TARGET=node

# 构建项目
RUN cnpm run build && ls -la

# 复制项目文件
COPY . .

# 暴露容器端口
EXPOSE 3000

# 启动命令
CMD ["npm", "start"]