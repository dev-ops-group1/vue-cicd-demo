import Vue from 'vue'
import VueI18n from 'vue-i18n'
//import { localRead } from '@/libs/util'
import customZhCn from './lang/zh-CN'
import customJaJp from './lang/ja-JP'
import customEnUs from './lang/en-US'
import zhCnLocale from 'view-design/src/locale/lang/zh-CN'
import enUsLocale from 'view-design/src/locale/lang/en-US'
import jaJpLocale from 'view-design/src/locale/lang/ja-JP'

Vue.use(VueI18n)

// 自动根据浏览器系统语言设置语言
// const navLang = navigator.language
const navLang = ''
const localLang = (navLang === 'zh-CN' || navLang === 'ja-JP' || navLang === 'en-US') ? navLang : false
let lang = localLang || localStorage.getItem('local') || 'zh-CN'

Vue.config.lang = lang

// vue-i18n 6.x+写法
Vue.locale = () => {}
const messages = {
  'zh-CN': Object.assign(zhCnLocale, customZhCn),
  'ja-JP': Object.assign(jaJpLocale, customJaJp),
  'en-US': Object.assign(enUsLocale, customEnUs)
}
const i18n = new VueI18n({
  locale: lang,
  messages
})
export default i18n

// 重新封装方法
export function $t(args) {
  return i18n.tc.call(i18n, args);
}

// 获取接口参数lang
export function getApiLang() {
  switch (i18n.locale) {
    case 'zh-CN':
      return 'zhCN'
    case 'ja-JP':
      return 'jaJP'
    case 'en-US':
      return 'enUS'
  }
  return ''
}

// vue-i18n 5.x写法
// Vue.locale('zh-CN', Object.assign(zhCnLocale, customZhCn))
// Vue.locale('en-US', Object.assign(zhTwLocale, customZhTw))
// Vue.locale('zh-TW', Object.assign(enUsLocale, customEnUs))
