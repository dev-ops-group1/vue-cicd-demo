import Vue from 'vue'
import App from './App'
import store from './store'
import iView from 'view-design'
import i18n from '@/locale'



import './index.less'


// 实际打包时应该不引入mock
/* eslint-disable */
// if (process.env.NODE_ENV !== 'production') require('@/mock')

Vue.use(iView, {
  i18n: (key, value) => i18n.t(key, value)
})

/**
 * @description 注册admin内置插件
 */

/**
 * @description 生产环境关掉提示
 */
Vue.config.productionTip = false
/**
 * @description 全局注册应用配置
 */

/**
 * 注册指令
 */


/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  store,
  render: h => h(App)
})

