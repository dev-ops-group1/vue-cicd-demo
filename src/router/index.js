import Vue from 'vue'
import Router from 'vue-router'
import { constantRouterMap, asyncRouterMap } from './routers'
import store from '@/store'
import iView from 'view-design'
import { setToken, getToken, canTurnTo } from '@/libs/util'
import stompManager from '@/libs/stompManager'
import config from '@/config'
const { homeName } = config
const wlRouters = ['message'] // 路由白名单

Vue.use(Router)
const router = new Router({
  routes: constantRouterMap,
  // mode: 'history'
})
const LOGIN_PAGE_NAME = 'login'

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

const turnTo = (to, access, next, replace) => {
  // if (to.name == homeName || to.name == LOGIN_PAGE_NAME) {
  //   turnToDefaultRoute(next)
  //   return;
  // }
  if (replace) {
    next({ ...to, replace: true })
  }else {
    const tmpPath = to.path.replace("/","")
    if (wlRouters.indexOf(to.name) > -1) { // 白名单内的路由，直接跳转
      next()
    }else if (to.name && ( store.getters.flatAllRouters.some(router => (router.name == to.name)) )) { // 使用扁平化后的所有可访问的路由数组查询-判断是否是可以正常访问
      next()
    }else if (
      (to.name && asyncRouterMap.some(router => (router.name == to.name) || (router.children && router.children.some(child => child.name == to.name))))
      || (to.path && asyncRouterMap.some(router => (router.name == tmpPath) || (router.children && router.children.some(child => child.name == tmpPath))))
      ) { // 判断是否是无权限访问
      next({ replace: true, name: 'error_401' }) 
    }else {
      next({ replace: true, name: 'error_404' }) 
    }
  }
}
const turnToDefaultRoute = (next) => {
  const defaultRoute = store.getters.defaultRoute
  if (defaultRoute) {
    next({
      name: defaultRoute.name
    }) 
    iView.LoadingBar.finish()
    return;
  }
  next()
}

router.beforeEach((to, from, next) => {
	// console.log('beforeEach---------', to)
  iView.LoadingBar.start()
  const token = getToken()
  // console.log('token---------',token)
  if (!token) { // 未登录
    // 断开websocket连接
    stompManager.disconnect()
    store.dispatch('setMsgCount', 0)
    if (to.name === LOGIN_PAGE_NAME) { // 要跳转到登录页
      next()
    }else { // 要跳转非登录页
      // 记录令牌失效时想要跳转的页面
      window.localStorage.setItem('toPage',JSON.stringify({fullPath:to.fullPath,hash:to.hash,meta:to.meta,name:to.name,params:to.params,path:to.path,query:to.query}))
      next({
        name: LOGIN_PAGE_NAME // 跳转到登录页
      })
    }
  }else { // 已登录
    if (store.state.user.hasGetInfo) {
      turnTo(to, store.state.user.access, next)
    } else { // 获取用户角色信息
      store.dispatch('getUserInfo').then(res => {
          // 拉取用户信息，通过用户权限和跳转的页面的name来判断是否有权限访问;access必须是一个数组，如：['super_admin'] ['super_admin', 'admin']
          // turnTo(to, user.access, next)
          store.dispatch('GenerateRoutes', { roles: res.obj.userJoinRoleJoinWhs }).then(() => { // 生成可访问的路由表(permission.js)文件
            // console.log('store.getters.addRouters---------', store.getters.addRouters)
            // store.getters.addRouters.forEach(route=>{ // 动态添加可访问路由表 (addRoutes方法Vue-router4已经弃用,使用该方法代替addRoutes方法)
            //   router.addRoute(route);
            // })
            router.addRoutes(store.getters.addRouters) // 动态添加可访问路由表 
            // next({ ...to, replace: true }) // hack方法 确保addRoutes已完成 ,set the replace: true so the navigation will not leave a history record
            // 连接websocket
            stompManager.init({
                userId: store.state.user.userId,
                type: 'WEB',
            })
            turnTo(to, store.state.user.access, next, true) // 设置为true 重定向重新加载路由-会再次触发路由守卫
          }).catch((err) => {
            console.log('获取菜单信息异常------');
            // setToken('')
            iView.LoadingBar.error()
            iView.Message.error(err)
            store.dispatch('handleLogOut')
            next({
              name: 'login'
            })
          })

      }).catch((err) => {
        console.log('获取用户信息异常------');
        // setToken('')
        iView.LoadingBar.error()
        iView.Message.error(err.msg)
        store.dispatch('handleLogOut')
        next({
          name: 'login'
        })
      })
    }
  }
})

router.afterEach(to => {
  iView.LoadingBar.finish()
  window.scrollTo(0, 0)
})

export default router
