import { getParams } from '@/libs/util'
const USER_MAP = {
  s_admin: {
    name: 's_admin',
    password: 'django123456',
    user_id: '1',
    access: ['s_admin', 'a_admin', 'c_admin'],
    token: 's_admin',
    avator: 'https://file.iviewui.com/dist/a0e88e83800f138b94d2414621bd9704.png'
  },
  a_admin: {
    name: 'a_admin',
    password: 'a_admin123456',
    user_id: '2',
    access: ['a_admin', 'c_admin'],
    token: 'a_admin',
    avator: 'https://avatars0.githubusercontent.com/u/20942571?s=460&v=4'
  },
  c_admin: {
    name: 'c_admin',
    password: 'c_admin123456',
    user_id: '3',
    access: ['c_admin'],
    token: 'c_admin',
    avator: 'https://avatars0.githubusercontent.com/u/20942571?s=460&v=4'
  }
}

export const login = req => {
  req = JSON.parse(req.body)
  if (USER_MAP[req.userName] && USER_MAP[req.userName].password == req.password) { // 用户密码验证成功
    return {
      status: 200,
      obj: USER_MAP[req.userName]
    }
  }
  return {
    status: 0,
    msg: '用户名或密码错误'
  }
  
  // return {token: USER_MAP[req.userName].token}
}

export const getUserInfo = req => {
  const params = getParams(req.url)
  return USER_MAP[params.token]
}

export const logout = req => {
  return null
}